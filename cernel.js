var REQ = {
	encUri: function(url, params){ // Encode URI request
		if(params === undefined){
			return url;
		}
		var urlStr = [];
		var enc = encodeURIComponent;
		for(var key in params){
			urlStr.push(enc(key) + "=" + enc(params[key]));
		}
		url += "?" + urlStr.join("&");
		return url;
	},
	urlApi: function(method){ // Get URI for method
		return "https://api.vk.com/method/" + method;
	},
	goReq: function(url, params, cb, body, onProgress){ // Post request on <url> with GET <params> and call <cb> after with JSON object as argument
		url = REQ.encUri(url, params);
		var req = new XMLHttpRequest();
		req.open(body === undefined ? "GET" : "POST", url, true);
		req.onreadystatechange = function(e){
			if(req.readyState != 4){
				return;
			}
			if(req.status != 200){
				console.error(req.statusText);
				return;
			}
			answ = req.responseText;
			try{
				answ = JSON.parse(answ);
			} catch(err){
				console.error(err);
				document.body.innerHTML = answ;
				return;
			}
			cb(answ);
		}
		if(onProgress !== undefined){
			req.onprogress = function(e){
				if(e.lengthComputable){
					onProgress(e.loaded, e.total);
				}
			}
		}
		req.send(body);
	},
	openTab: function(url, cb){
		chrome.tabs.create({url: url}, function(tab){
			cb(tab.id);
		});
	},
	openWin: function(url, cb){
		chrome.windows.create({
			url: url,
			type: "popup"
		}, function(w){
			if(cb !== undefined){
				cb(w.tabs[0].id, w);
			}
		});
	}
};

var AUTCH = {
	key: "fake_key",
	vers: 5.84,
	// vers: 5.92,
	appl_id: 6685174,
	// appl_perm: 268435455, // ALL
	appl_perm: 4096 + 65536,
	getURL: function(){
		var url = REQ.encUri("https://oauth.vk.com/authorize", {
			client_id: AUTCH.appl_id,
			redirect_uri: "https://oauth.vk.com/blank.html",
			response_type: "token",
			display: "page",
			v: AUTCH.vers,
			state: "123",
			scope: AUTCH.appl_perm
		});
		return url;
	},
	newKeyFromUrl: function(url){
		AUTCH.key = url.split("#")[1].split("access_token=")[1].split("&")[0];
	},
	newKey: function(cb){
		REQ.openWin(AUTCH.getURL(), function(tabId){
			var ourTab = tabId;
			function lis(tabId, stuff, tab){
				if(tabId != ourTab){
					return;
				}
				if(tab.url.indexOf("https://oauth.vk.com/blank.html") == -1){
					return;
				}
				chrome.tabs.onUpdated.removeListener(lis);
				var url = tab.url;
				chrome.tabs.remove(ourTab, function(){});
				AUTCH.newKeyFromUrl(url);
				if(cb !== undefined){
					cb();
				}
			}
			chrome.tabs.onUpdated.addListener(lis);
		});
	},
	callAPI: function(func, params, cb, onError){
		params.access_token = AUTCH.key;
		params.v = AUTCH.vers;
		REQ.goReq(REQ.urlApi(func), params, function(res){
			if(res.error !== undefined){
				var err = res.error;
				switch(err.error_code){
				case 5: // Wrong access token
					AUTCH.newKey(function(){
						AUTCH.callAPI(func, params, cb);
					});
					break;
				default: // Other errors
					if(onError === undefined){
						console.error(err);
					} else {
						onError(err);
					}
					break;
				}
				return;
			}
			cb(res.response);
		});
	}
};
