chrome.extension.onMessage.addListener(function(req, sendr, cb){
	if(req.action != 'getWebPic'){
		return;
	}
	var pics = [];
	function onSnap(){
		dumpFile(function(blob){
			pics.push(blob);
			if(pics.length == 10){
				video.onclick = null;
				video.style.cursor = "not-allowed";
			}
		});
	}
	function onSend(){
		var res = [];
		for(var i = 0; i < pics.length; i++){
			res.push(getPortFile(pics[i]));
		}
		cb(res);
		window.blur();
	}
	reqWebCam(function(){
		video.onclick = onSnap;
		sender.onclick = onSend;
	});
	return true;
});

function getPortFile(file, cb){
	return {
		name: (new Date().getTime()) + ".png",
		url: URL.createObjectURL(file)
	};
}

function reqWebCam(cb){
	video.style.cursor = "wait";
	navigator.getUserMedia({video: true}, function(stream){
		video.srcObject = stream;
		video.play().then(function(){
			video.style.cursor = "pointer";
			cb();
		});
		
	}, function(err){
		console.error("Cannot WEB > " + err);
	});
}

function dumpFile(cb){
	video.style.cursor = "wait";
	var ctx = document.createElement('canvas').getContext('2d');
	var sizes = [
		video.videoWidth,
		video.videoHeight
	];
	ctx.canvas.width = sizes[0];
	ctx.canvas.height = sizes[1];
	ctx.drawImage(video, 0, 0, sizes[0], sizes[1]);
	ctx.canvas.toBlob(function(blob){
		video.style.cursor = "pointer";
		ctx.canvas.style.width = "100px";
		pics.appendChild(ctx.canvas);
		cb(blob);
	});
}
