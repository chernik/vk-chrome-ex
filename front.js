var FRONT = {
	sendMsg: function(data, cb){
		chrome.extension.sendMessage(data, function(res){
			if(res === undefined){
				throw "I have empty message";
			}
			cb(res);
		});
	},
	newEl: function(tag, parent){
		var d = document.createElement(tag);
		if(parent !== undefined){
			parent.appendChild(d);
		}
		return d;
	},
	clearLog: function(){
		FRONT.logDiv.innerHTML = "";
	},
	log: function(text){
		FRONT.newEl('hr', FRONT.logDiv);
		var span = FRONT.newEl('span', FRONT.logDiv);
		span.innerText = text;
	},
	getText: function(){
		return FRONT.textInput.value;
	},
	getPortFile: function(file, cb){
		return {
			name: file.name,
			url: URL.createObjectURL(file)
		};
	},
	getUserFile: function(cb){
		var inp = FRONT.newEl("input");
		inp.type = "file";
		inp.onchange = function(e){
			var file = inp.files[0];
			if(file === undefined){
				return;
			}
			cb(file);
		}
		inp.click();
	},
	getUserPortFile: function(cb){
		FRONT.getUserFile(function(file){
			cb(FRONT.getPortFile(file));
		});
	},
	reloadCurrentTab: function(cb){
		cb();
		/* // TODO: Do we need it?
		chrome.tabs.get(FRONT.currentTab.id, function(tab){
			FRONT.currentTab = tab;
			cb();
		}
		*/
	},
	getSelectedPeerId: function(cb){
		function getPeerId(){
			var url = FRONT.currentTab.url;
			var sel;
			try {
				sel = url.split("sel=")[1].split("&")[0];
			} catch(err){
				return null;
			}
			if(sel[0] == "c"){
				sel = +sel.slice(1) + 2000000000;
			} else {
				sel = +sel;
			}
			return sel;
		}
		FRONT.reloadCurrentTab(function(){
			cb(getPeerId());
		});
	},
	clearMes: function(){
		FRONT.mesDiv.innerHTML = "";
	},
	addFirst: function(par, ch){
		var fch = par.children[0];
		if(fch === undefined){
			par.appendChild(ch);
		} else {
			par.insertBefore(ch, fch);
		}
	},
	addMes: function(mes, user){
		var div = FRONT.newEl("div");
		FRONT.addFirst(FRONT.mesDiv, div);
		FRONT.newEl("hr", div);
		if(mes.text !== undefined && mes.text != ""){
			var txt = FRONT.newEl("div", div);
			txt.style.width = "450px";
			txt.style.overflow = "auto";
			txt.innerText = mes.text;
		}
		if(mes.audio !== undefined){
			var au = FRONT.newEl("audio", div);
			au.src = mes.audio;
			au.controls = true;
			au.classList.add("audioMes");
			au.playbackRate = 1.4;
			var lastAu = FRONT.mesDiv.getElementsByClassName("audioMes")[1];
			if(lastAu !== undefined){
				au.onended = function(e){
					lastAu.play();
				}
			}
		}
		if(mes.fromId !== undefined){
			var l = FRONT.newEl("a", div);
			l.href = "#";
			if(user !== undefined){
				l.innerHTML = user.name;
			} else {
				l.innerHTML = mes.fromId;
			}
			l.style.display = "block";
			l.onclick = function(e){
				FRONT.openWin(mes.fromId < 0 ? "https://vk.com" : "https://vk.com/id" + mes.fromId);
			}
		}
		if(mes.pics !== undefined && mes.pics.length > 0){
			var divPic = FRONT.newEl("div", div);
			function newL(i){
				return function(e){
					FRONT.openWin(mes.pics[i]);
				}
			}
			for(var i = 0; i < mes.pics.length; i++){
				var img = FRONT.newEl("img", divPic);
				img.style.width = "200px";
				img.src = mes.pics[i];
				img.onclick = newL(i);
				img.style.cursor = "help";
			}
		}
	},
	showMes: function(){
		FRONT.mesDiv.style.display = "";
	},
	offsetMes: 0,
	openWin: function(url, cb){
		chrome.windows.create({
			url: url,
			type: "popup"
		}, function(w){
			if(cb !== undefined){
				cb(w.tabs[0].id, w.id);
			}
		});
	},
	reportFile: function(portFile, cb){
		var req = new XMLHttpRequest();
		req.responseType = "blob";
		req.onreadystatechange = function(){
			if(req.readyState != 4){
				return;
			}
			if(req.status != 200){
				console.error(req.statusText);
				return;
			}
			var blob = req.response;
			URL.revokeObjectURL(portFile.url);
			var file = new File([blob], portFile.name, {type: blob.type});
			cb(FRONT.getPortFile(file));
		}
		req.open("GET", portFile.url);
		req.send();
	},
	reportFiles: function(portFiles, cb){
		var all = portFiles.length;
		var result = [];
		var now = 0;
		function onNext(pos){
			return function(portFile){
				result[pos] = portFile;
				now++;
				if(now == all){
					cb(result);
				}
			}
		}
		for(var i = 0; i < all; i++){
			FRONT.reportFile(portFiles[i], onNext(i));
		}
	}
};

var BUTTONS = {
	sendAudio: function(){
		FRONT.clearLog();
		FRONT.getUserPortFile(function(portFile){
			FRONT.getSelectedPeerId(function(peerId){
				if(peerId == null){
					FRONT.log("Please select the dialog!");
					return;
				}
				FRONT.sendMsg({
					action: 'sendAudioMessage',
					portFile: portFile,
					peerId: peerId,
					text: FRONT.getText()
				}, function(res){
					FRONT.log(JSON.stringify(res));
				});
			});
		});
	},
	loadAudios: function(){
		FRONT.clearLog();
		FRONT.getSelectedPeerId(function(peerId){
			if(peerId == null){
				FRONT.log("Please select the dialog!");
				return;
			}
			FRONT.sendMsg({
				action: 'loadAudioMessages',
				peerId: peerId,
				offset: FRONT.offsetMes
			}, function(res){
				if(res.error !== undefined){
					FRONT.log(JSON.stringify(res.error));
					return;
				}
				var sz = FRONT.mesDiv.scrollHeight - FRONT.mesDiv.scrollTop;
				for(var i = 0; i < res.messages.length; i++){
					FRONT.addMes(res.messages[i], res.users[res.messages[i].fromId]);
				}
				if(FRONT.offsetMes == 0){
					FRONT.showMes();
				}
				FRONT.offsetMes += res.messages.length;
				FRONT.mesDiv.scrollTop = FRONT.mesDiv.scrollHeight - sz;
			});
		});
	},
	sendWebPic: function(){
		FRONT.clearLog();
		FRONT.getSelectedPeerId(function(peerId){
			if(peerId == null){
				FRONT.log("Please select the dialog!");
				return;
			}
			FRONT.openWin(chrome.runtime.getURL('webpic.htm'), function(tabId, winId){
				function onTab(tabNow, state){
					if(tabNow != tabId){
						return;
					}
					if(state.status != "complete"){
						return;
					}
					chrome.tabs.onUpdated.removeListener(onTab);
					chrome.tabs.sendMessage(tabId, {
						action: 'getWebPic'
					}, function(result){
						if(result === undefined){
							FRONT.log('Canceled');
							return;
						}
						FRONT.reportFiles(result, function(pics){
							chrome.windows.remove(winId);
							FRONT.sendMsg({
								peerId: peerId,
								text: FRONT.getText(),
								pics: pics,
								action: 'sendWebPic'
							}, function(res){
								FRONT.log(JSON.stringify(res));
							});
						});
					});
				}
				chrome.tabs.onUpdated.addListener(onTab);
			});
		});	
	}
}

window.onload = function(e){
	FRONT.logDiv = document.getElementById("logDiv");
	FRONT.textInput = document.getElementById("textInput");
	FRONT.mesDiv = document.getElementById("mesDiv");
	FRONT.mesDiv.style.display = "none";
	FRONT.videoEl = document.getElementById("videoEl");
	chrome.runtime.onMessage.addListener(function(req, sender, cb){
		if(req === undefined){
			return;
		}
		req = JSON.stringify(req);
		FRONT.log(req);
		cb("recv");
	});
	chrome.tabs.query({
		active: true,
		currentWindow: true
	}, function(tabs){
		FRONT.currentTab = tabs[0];
	});
	for(var key in BUTTONS){
		document.getElementById(key).onclick = BUTTONS[key];
	}
}