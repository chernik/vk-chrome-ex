var LISTENERS = {
	sendAudioMessage: function(req, cb, cbMiddle){
		if(req.text === undefined){
			req.text = "";
		}
		DOCS.uploadPortDoc(req.portFile, "a", function(state){
			if(state.fileStr === undefined){
				cbMiddle({
					state: state
				});
				return;
			}
			MESSAGES.send({
				text: req.text,
				att: [state.fileStr],
				peerId: req.peerId
			}, cb);
		});
	},
	loadAudioMessages: function(req, cb, cbMiddle){
		if(req.offset === undefined){
			req.offset = 0;
		}
		MESSAGES.get(req.peerId, req.offset, function(mes){
			function mesMap(mes){
				if(mes.att.audio_message !== undefined){
					return {
						audio: mes.att.audio_message[0].link,
						text: mes.text,
						fromId: mes.fromId
					};
				}
				return {
					text: mes.text,
					fromId: mes.fromId,
					pics: mes.att.photo === undefined ? [] : mes.att.photo.map(el => el.link)
				}
			}
			cb({
				messages: mes.messages.map(mesMap),
				users: mes.users
			});
		});
	},
	sendWebPic: function(req, cb, cbMiddle){
		DOCS.uploadPortMesPics(req.pics, req.peerId, function(state){
			if(state.filesStr === undefined){
				cbMiddle(state);
				return;
			}
			MESSAGES.send({
				text: req.text,
				att: state.filesStr,
				peerId: req.peerId
			}, cb);
		});
	}
}

chrome.runtime.onMessage.addListener(function(req, sender, cb){
	function cbMiddle(data){
		chrome.runtime.sendMessage(sender.id, data, console.log);
	}
	function cbEnd(data){
		try {
			cb(data);
		} catch(err) {
			console.info("We lose the window...");
		}
	}
	cbMiddle("recv");
	var f = req.action;
	f = LISTENERS[f];
	if(f === undefined){
		cbEnd({
			error: 'Wrong action'
		});
		return;
	}
	f(req, cbEnd, cbMiddle);
	return true;
});