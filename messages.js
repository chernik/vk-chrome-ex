var DOCS = {
	uploadDoc: function(file, params, callback){
		function getUploadServer(cb){
			var paramsO = {};
			if(params.indexOf("a") != -1){
				paramsO.type = "audio_message";
			}
			AUTCH.callAPI("docs.getUploadServer", paramsO, function(res){
				cb(res.upload_url);
			});
		}
		function startUpload(url, body, cb){
			REQ.goReq(url, undefined, function(res){
				if(res.file === undefined){
					console.error(res);
					return;
				}
				cb(res.file);
			}, body, function(loaded, total){
				callback({
					progress: {
						loaded: loaded,
						total: total
					}
				});
			});
		}
		function packName(doc){
			return "doc" + doc.owner_id + "_" + doc.id;
		}
		function saveFile(fileStr, cb){
			AUTCH.callAPI("docs.save", {
				"file": fileStr
			}, function(res){
				cb(packName(res[0]));
			});
		}
		function packFile(){
			var fd = new FormData();
			fd.append("file", file);
			return fd;
		}
		getUploadServer(function(url){
			startUpload(url, packFile(), function(fileStr){
				saveFile(fileStr, function(fileStr){
					callback({
						fileStr: fileStr
					});
				});
			});
		});
	},
	uploadMesPic: function(file, peerId, callback){
		function getUploadServer(cb){
			var paramsO = {
				peer_id: peerId
			};
			AUTCH.callAPI("photos.getMessagesUploadServer", paramsO, function(res){
				cb(res.upload_url);
			});
		}
		function startUpload(url, body, cb){
			REQ.goReq(url, undefined, function(res){
				if(res.photo === undefined || res.photo == "[]"){
					console.error(res);
					return;
				}
				cb(res);
			}, body, function(loaded, total){
				callback({
					progress: {
						loaded: loaded,
						total: total
					}
				});
			});
		}
		function packName(doc){
			// access_key
			return "photo" + doc.owner_id + "_" + doc.id;
		}
		function saveFile(fileData, cb){
			AUTCH.callAPI("photos.saveMessagesPhoto", fileData, function(res){
				cb(packName(res[0]));
			});
		}
		function packFile(){
			var fd = new FormData();
			fd.append("photo", file);
			return fd;
		}
		getUploadServer(function(url){
			startUpload(url, packFile(), function(fileData){
				saveFile(fileData, function(fileStr){
					callback({
						fileStr: fileStr
					});
				});
			});
		});
	},
	getUserFile: function(portFile, cb){
		var req = new XMLHttpRequest();
		req.responseType = "blob";
		req.onreadystatechange = function(){
			if(req.readyState != 4){
				return;
			}
			if(req.status != 200){
				console.error(req.statusText);
				return;
			}
			var blob = req.response;
			URL.revokeObjectURL(portFile.url);
			var file = new File([blob], portFile.name, {type: blob.type});
			cb(file);
		}
		req.open("GET", portFile.url);
		req.send();
	},
	uploadPortDoc: function(portFile, params, cb){
		DOCS.getUserFile(portFile, function(file){
			DOCS.uploadDoc(file, params, cb);
		});
	},
	uploadPortMesPics: function(portFiles, peerId, cb){
		var res = [];
		function goNext(pos){
			if(pos == portFiles.length){
				cb({
					filesStr: res
				});
				return;
			}
			DOCS.getUserFile(portFiles[pos], function(file){
				DOCS.uploadMesPic(file, peerId, function(f){
					if(f.fileStr === undefined){
						cb(f);
						return;
					}
					cb({
						loadedFile: pos
					});
					res.push(f.fileStr);
					goNext(pos + 1);
				});
			});
		}
		goNext(0);
	}
}

var MESSAGES = {
	send: function(mess, cb){
		var req = {
			attachment: mess.att.join(","),
			peer_id: mess.peerId,
			message: mess.text
		};
		if(mess.keyboard !== undefined){
			req.keyboard = mess.keyboard;
		}
		AUTCH.callAPI("messages.send", req, function(res){
			if(cb !== undefined){
				cb({mesId: res});
			}
		});
	},
	sendWithKB: function(mess, keyboard, cb, isOnce){
		if(isOnce === undefined){
			isOnce = true;
		}
		function parseBUT(but){
			var title = but.split("[")[0];
			var color = but.split("[")[1].split("]")[0];
			var colors = {
				blue: "primary",
				white: "default",
				red: "negative",
				green: "positive"
			}
			if(color in colors){
				color = colors[color];
			} else {
				color = colors.white;
			}
			return {
				action: {
					type: "text",
					label: title
				},
				color: color
			};
		}
		function parseKB(){
			keyboard = keyboard.split("\n");
			for(var i = 0; i < keyboard.length; i++){
				keyboard[i] = keyboard[i].split(";").map(parseBUT);
			}
			return JSON.stringify({
				one_time: isOnce,
				buttons: keyboard
			});
		}
		mess.keyboard = parseKB();
		MESSAGES.send(mess, cb);
	},
	get: function(peerId, offset, cb){
		function packFileStr(att, type){
			return type + att.owner_id + "_" + att.id; // + "_" + att.access_key;
		}
		var parsers = {
			audio_message: function(doc){
				return {
					link: doc.link_mp3,
					fileStr: packFileStr(doc, "doc")
				}
			},
			photo: function(pic){
				pic.sizes.sort(function(a, b){
					return a.width - b.width;
				});
				return {
					link: pic.sizes.slice(-1)[0].url,
					albumId: pic.album_id,
					fileStr: packFileStr(pic, "photo")
				}
			},
			link: function(link){
				return {
					texts: {
						caption: link.caption,
						title: link.title
					},
					link: link.url,
					photo: link.photo === undefined ? null : parsers.photo(link.photo)
				}
			}
		}
		function mapMes(mes){
			var res = {
				mesId: mes.id,
				text: mes.text,
				fromId: mes.from_id,
				date: new Date(mes.date * 1000),
				att: {}
			};
			var att = mes.attachments;
			for(var i = 0; i < att.length; i++){
				if(res.att[att[i].type] === undefined){
					res.att[att[i].type] = [];
				}
				res.att[att[i].type].push(att[i][att[i].type]);
			}
			function newMap(key){
				return function(att){
					var res;
					if(key in parsers){
						res = parsers[key](att);
					} else {
						res = {
							fileStr: packFileStr(att, key)
						};
					}
					return res;
				}
			}
			for(var key in res.att){
				res.att[key] = res.att[key].map(newMap(key));
			}
			res.orig = mes;
			return res;
		}
		function collectIds(mes){
			var ids = new Set();
			for(var i = 0; i < mes.length; i++){
				ids.add(mes[i].fromId);
			}
			return Array.from(ids);
		}
		function getUsers(ids, cb){
			AUTCH.callAPI("users.get", {
				user_ids: ids.join(","),
				fields: ["id", "first_name", "last_name"].join(",")
			}, function(res){
				res = res.map(el => ({
					name: el.first_name + " " + el.last_name,
					id: el.id
				}));
				var users = {};
				for(var i = 0; i < res.length; i++){
					users[res[i].id] = res[i];
					delete(res[i].id);
				}
				cb(users);
			});
		}
		AUTCH.callAPI("messages.getHistory", {
			count: 200,
			peer_id: peerId,
			extended: 1,
			offset: offset
		}, function(res){
			var mes = res.items.map(mapMes);
			var ids = collectIds(mes);
			getUsers(ids, function(users){
				cb({
					messages: mes,
					users: users
				});
			});
		});
	}
}
